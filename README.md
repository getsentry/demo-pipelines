# Notify Sentry of a Release

An example script for notifying Sentry of a new release created by Bitbucket Pipelines.


## How to use it

* Add required environment variables to your Bitbucket environment variables.
* Copy `notify-sentry-of-release.bash` to your project.
* Add `./notify-sentry-of-release.bash` to your build configuration.


## Required environment variables

* **`SENTRY_API_KEY`**: (Required) API Key for the target Sentry account.
* **`SENTRY_ORGANIZATION`**: (Optional) Slug (aka name) for the Sentry organization. If not provided, this script assumes Sentry matches Bitbucket organization. Example: "demo"
* **`SENTRY_PROJECT`**: (Optional) Slug (aka name) for the Sentry project. If not provided, this script assumes Sentry matches Bitbucket repository. Example: "ZeroDivisionError"
* **`BITBUCKET_COMMIT`**: (Provided) Commit SHA that triggered the build.